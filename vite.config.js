import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  proxy: {
    '/__vitest__': {
      target: 'http://localhost:3000',
      rewrite: (path) => path.replace(/^\/__vitest__/, ""),
      changeOrigin: true,
  },
  },
  server: {
    host: true, // 显示IP位置
    strictPort: true,
    watch: {
      usePolling: true
    },
    port: 3000,
    open: false,
  },
})
